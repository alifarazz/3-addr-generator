%{
#include "y.tab.h"
#include <stdlib.h>
#include <string.h>
    void yyerror(const char *);


%}
%option noyywrap

/* definitions */

digit				 [0-9]
letter			 [A-Za-z]
whitespace 	 [ \n\t]

digits			 {digit}+
letters			 {letter}+
delim			 	 {whitespace}+

ident				 ({letter}|"_")({digit}|{letter}|"_")*
integer		   [\+\-]?(0x|0|0b)?({digits})(u)?
floatnum  	 [-+]?{digit}*\.?{digits}([Ee][-+]?{digits})?
%%

{integer}    {strncpy(yylval.str, yytext, 128); return INTEGER;}
{floatnum}   {strncpy(yylval.str, yytext, 128); return FLOAT;}

"<="         {strncpy(yylval.str, yytext, 128); return LTE;}
">="         {strncpy(yylval.str, yytext, 128); return GTE;}
"=="         {strncpy(yylval.str, yytext, 128); return EQ; }
"!="         {strncpy(yylval.str, yytext, 128); return NEQ;}

"&&"         {strncpy(yylval.str, yytext, 128); return LGL_AND;}
"||"         {strncpy(yylval.str, yytext, 128); return LGL_OR; }
"^^"         {strncpy(yylval.str, yytext, 128); return LGL_XOR;}
"!"          {strncpy(yylval.str, yytext, 128); return LGL_NOT;}

"&"          {strncpy(yylval.str, yytext, 128); return BIN_AND;}
"|"          {strncpy(yylval.str, yytext, 128); return BIN_OR; }
"^"          {strncpy(yylval.str, yytext, 128); return BIN_XOR;}
"~"          {strncpy(yylval.str, yytext, 128); return BIN_NOT;}

"<"          {strncpy(yylval.str, yytext, 128); return LT; }
">"          {strncpy(yylval.str, yytext, 128); return GT; }

[\-\+/\*{}();,=%]      {return yytext[0];}

"if"         {return COND_IF;}
"while"      {return LOOP_WHILE;}
"for"        {return LOOP_FOR;}
"int"        {return TYPE_INT;}
"float"      {return TYPE_FLOAT;}

{ident}	     {strncpy(yylval.str, yytext, 128); return VARIABLE;}

{whitespace} {}
.     {yyerror("ERROR:invalid input");}

%%
